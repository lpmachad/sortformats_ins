#!/usr/bin/env python3

"""Ordena una lista de formatos según su nivel de compresión.
Utiliza el algoritmo de inserción"""

import sys

# Lista ordenada de formatos de imagen, de menor a mayor inserción
# (clasificación asumida)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")

def menor_que(formato1: str, formato2: str) -> bool:
    """Determina si formato1 es menor que formato2.
    Devuelve True si formato1 es menor, False en caso contrario.
    Un formato es menor que otro si aparece antes en la lista fordered.
    """
    return fordered.index(formato1) < fordered.index(formato2)

def encontrar_pos_menor(formatos: list, pivote: int) -> int:
    """Encuentra el formato menor en formatos después del pivote.
    Devuelve el índice del formato menor encontrado."""
    menor = pivote
    for pos in range(pivote, len(formatos)):
        if menor_que(formatos[pos], formatos[menor]):
            menor = pos
    return menor

def ordenar_formatos(formatos: list) -> list:
    """Ordena la lista de formatos.
    Devuelve la lista ordenada de formatos, según su posición en fordered."""
    for pos_pivote in range(len(formatos)):
        pos_menor: int = encontrar_pos_menor(formatos, pos_pivote)
        if pos_menor != pos_pivote:
            (formatos[pos_pivote], formatos[pos_menor]) = \
                (formatos[pos_menor], formatos[pos_pivote])
    return formatos

def main():
    """Lee los argumentos de la línea de comandos y los imprime ordenados.
    Además, verifica si son formatos válidos utilizando la tupla fordered."""
    formatos: list = sys.argv[1:]
    for formato in formatos:
        if formato not in fordered:
            sys.exit(f"Formato inválido: {formato}")
    formatos_ordenados: list = ordenar_formatos(formatos)
    for formato in formatos_ordenados:
        print(formato, end=" ")
    print()

if __name__ == '__main__':
    main()

